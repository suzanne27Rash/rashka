"use strict";

const express = require('express');
const router = express.Router();
const knex = require('../db/knex');


router.get('/', (req, res) => {
    knex('books')
        .then((books) => {
            res.render("books/all", {
                books: books
            });
        });
});

router.get('/new', (req, res) => {
    res.render('books/new')
});

// req.params.id contains the id passed into the route when it is called in the browser (http://localhost:3000/books/1) req.params.id = 1
router.get('/:id', (req, res) => {
    console.log(req.params.id);
    knex('books')
        .where('id', req.params.id)
        // .first() dont use first if your ejs has a forEach, because it returns a singular object rather than an array
        .then((books) => {
            if (!books) {
                res.redirect('/books')
            } else {
                res.render("books/all", {
                    books: books
                });
            }
        });
});

router.post('/', (req, res) => {
    knex('books').insert({
            title: req.body.title,
            genre: req.body.genre,
            description: req.body.description,
            cover_url: req.body.cover_url
        })
        .then(() => {
            res.redirect('/books');
        })
})

module.exports = router;
