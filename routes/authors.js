"use strict";

const express = require('express');
const router = express.Router();
const knex = require('../db/knex');


router.get('/', (req, res) => {
    knex('authors')
        .then((authors) => {
            res.render('authors/all', {
                authors: authors
            }); //ejs reference 'author/all, {authors: is object being sent to : authors on ejs page}'
        });
});

router.get('/new', (req, res) => {
    res.render('authors/new')
});

router.get('/:id', (req, res) => {
    knex('authors')
        .where('id', req.params.id)
        .then((authors) => {
            res.render('authors/all', {
                authors: authors
            });
        });
});


module.exports = router;
